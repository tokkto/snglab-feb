import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../shared';
import {DialogModule, TabMenuModule, DropdownModule} from 'primeng/primeng';

import { ControlInformacionService,
    ControlInformacionComponent,
    ControlInformacionRoute,
} from './';
import { FormularioPerfilComponent } from '../formulario-perfil/index';
import { ActieconService,
        ActieconComponent,
        actieconRoute,
} from '../../../entities/actiecon';
import { FormularioPerfil2Component } from '../formulario-perfil/formulario-perfil2.component';
import { FormularioPerfil3Component } from '../formulario-perfil/formulario-perfil3.component';
import { FormularioPerfil4Component } from '../formulario-perfil/formulario-perfil4.component';
import { FormularioPerfil5Component } from '../formulario-perfil/formulario-perfil5.component';
import { FormularioPerfil6Component } from '../formulario-perfil/formulario-perfil6.component';
import { FormularioFinancieroPrivadoN1Component,
    FormularioFinancieroPrivadoAnexo1AComponent,
    FormularioFinancieroPrivadoAnexo1BComponent,
    FormularioFinancieroPrivadoAnexo1CComponent,
    FormularioFinancieroPrivadoAnexo1DComponent } from '../formulario-financiero-privado/index';
import { FormfinancService, FormfinancDetalleService } from '../entities/index';
import { SolicitudComponent, SolicitudService } from '../entities/entities/solicitud/index';
import { SolicformComponent, SolicformService } from '../entities/entities/solicform/index';
import { DireccionComponent, DireccionService } from '../entities/entities/direccion/index';
import { FormperfilComponent, FormperfilService } from '../entities/entities/formperfil/index';
import { NegocolectComponent, NegocolectService } from '../entities/entities/negocolect/index';
import { ResulnegocComponent, ResulnegocService } from '../entities/entities/resulnegoc/index';
import { RespinformaComponent, RespinformaService } from '../entities/entities/respinforma/index';
import { AnexlaboralComponent } from '../entities/entities/anexlaboral/anexlaboral.component';
import { AnexlaboralService } from '../entities/entities/anexlaboral/anexlaboral.service';

const ENTITY_STATES = [
    ...ControlInformacionRoute,
];

@NgModule({
    declarations: [
        ControlInformacionComponent,
        SolicitudComponent,
        SolicformComponent,
        DireccionComponent,
        FormperfilComponent,
        FormularioPerfilComponent,
        FormularioPerfil2Component,
        FormularioPerfil3Component,
        FormularioPerfil4Component,
        FormularioPerfil5Component,
        FormularioPerfil6Component,
        FormularioFinancieroPrivadoN1Component,
        FormularioFinancieroPrivadoAnexo1AComponent,
        FormularioFinancieroPrivadoAnexo1BComponent,
        FormularioFinancieroPrivadoAnexo1CComponent,
        FormularioFinancieroPrivadoAnexo1DComponent,
        ActieconComponent,
        NegocolectComponent,
        ResulnegocComponent,
        RespinformaComponent,
        AnexlaboralComponent,
    ],
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES),
        DialogModule,
        TabMenuModule,
        DropdownModule,
    ],
    entryComponents: [
        ControlInformacionComponent
    ],
    providers: [
        ControlInformacionService,
        DireccionService,
        SolicitudService,
        SolicformService,
        FormperfilService,
        ActieconService,
        NegocolectService,
        ResulnegocService,
        RespinformaService,
        AnexlaboralService,
        FormfinancService,
        FormfinancDetalleService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayControlInformacionModule {}
