import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../../../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ResulnegocComponent } from './resulnegoc.component';

export const resulnegocRoute: Routes = [
    {
        path: 'resulnegoc',
        component: ResulnegocComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.resulnegoc.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
