import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../../../shared';
import {
    DireccionService,
    DireccionComponent,
    direccionRoute,
} from './';

const ENTITY_STATES = [
    ...direccionRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        DireccionComponent,
    ],
    entryComponents: [
        DireccionComponent,
    ],
    providers: [
        DireccionService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayDireccionModule {}
