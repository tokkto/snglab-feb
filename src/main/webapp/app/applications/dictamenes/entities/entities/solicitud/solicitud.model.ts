import { BaseEntity } from '../../../../../shared';

export class Solicitud implements BaseEntity {
    constructor(
        public id?: number,
        public nCodrepre?: number,
        public nCodsolic?: number,
        public nFlgactivo?: boolean,
        public nFlgestado?: number,
        public nFlgprorro?: number,
        public nSedereg?: number,
        public nSedeupd?: number,
        public tFecenvio?: any,
        public tFecnotif?: any,
        public tFecplazo?: any,
        public tFecprorro?: any,
        public tFecreg?: any,
        public tFecsolic?: any,
        public tFecsubsan?: any,
        public tFecupd?: any,
        public vArbitro?: string,
        public vCodarbit?: string,
        public vCodemple?: string,
        public vCodsindi?: string,
        public vCodsolic?: string,
        public vCodusu?: string,
        public vEmpleador?: string,
        public vNumexpedi?: string,
        public vRegistro?: string,
        public vRucsol?: string,
        public vSindicato?: string,
        public vSolicita?: string,
        public vUsuareg?: string,
        public vUsuaupd?: string,
        public vVoucher?: string,
        public reporteRes?: BaseEntity,
        public solFormularios?: BaseEntity[],
        public usuSolicitud?: BaseEntity[],
        public vNombres?: string
    ) {
        this.nFlgactivo = false;
    }
}
