import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../../../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { FormarchivoComponent } from './formarchivo.component';

export const formarchivoRoute: Routes = [
    {
        path: 'formarchivo',
        component: FormarchivoComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.formarchivo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
