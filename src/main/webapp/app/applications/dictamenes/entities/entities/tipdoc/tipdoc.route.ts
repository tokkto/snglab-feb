import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../../../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { TipdocComponent } from './tipdoc.component';

export const tipdocRoute: Routes = [
    {
        path: 'tipdoc',
        component: TipdocComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.tipdoc.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
