import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../../../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { OrganizacioComponent } from './organizacio.component';

export const organizacioRoute: Routes = [
    {
        path: 'organizacio',
        component: OrganizacioComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.organizacio.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
