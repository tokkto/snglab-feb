import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../../../shared';
import {
    FormarchivoService,
    FormarchivoComponent,
    formarchivoRoute,
} from './';

const ENTITY_STATES = [
    ...formarchivoRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        FormarchivoComponent,
    ],
    entryComponents: [
        FormarchivoComponent,
    ],
    providers: [
        FormarchivoService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayFormarchivoModule {}
