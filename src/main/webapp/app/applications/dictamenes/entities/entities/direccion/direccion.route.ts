import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../../../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DireccionComponent } from './direccion.component';

export const direccionRoute: Routes = [
    {
        path: 'direccion',
        component: DireccionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.direccion.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
