import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../../../shared';
import {
    ParticipaService,
    ParticipaComponent,
    participaRoute,
} from './';

const ENTITY_STATES = [
    ...participaRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ParticipaComponent,
    ],
    entryComponents: [
        ParticipaComponent,
    ],
    providers: [
        ParticipaService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayParticipaModule {}
