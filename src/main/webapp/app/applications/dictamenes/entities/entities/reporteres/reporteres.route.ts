import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../../../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ReporteresComponent } from './reporteres.component';

export const reporteresRoute: Routes = [
    {
        path: 'reporteres',
        component: ReporteresComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.reporteres.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
