import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';
import { Solicitud } from './solicitud.model';
import { ResponseWrapper, createRequestOption } from '../../../../../shared';
import { DictamenesConstants } from '../../index';
import { SERVER_API_URL } from '../../../../../app.constants';

@Injectable()
export class SolicitudService {

    private constants: DictamenesConstants;
    private resourceDictamenes: string;
    private resourceUrl: string;
    private resourceSearchUrl: string;

    constructor(private http: Http, private dateUtils: JhiDateUtils) {
        this.constants = new DictamenesConstants();
        this.resourceDictamenes = this.constants.DICTAMENES_RESOURCE;
        this.resourceUrl = SERVER_API_URL + this.resourceDictamenes + '/api/solicituds';
        this.resourceSearchUrl = SERVER_API_URL + this.resourceDictamenes + '/api/_search/solicituds';
    }

    create(solicitud: Solicitud): Observable<Solicitud> {
        const copy = this.convert(solicitud);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(solicitud: Solicitud): Observable<Solicitud> {
        const copy = this.convert(solicitud);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Solicitud> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Solicitud.
     */
    private convertItemFromServer(json: any): Solicitud {
        const entity: Solicitud = Object.assign(new Solicitud(), json);
        entity.tFecnotif = this.dateUtils.convertDateTimeFromServer(json.tFecnotif);
        entity.tFecplazo = this.dateUtils.convertDateTimeFromServer(json.tFecplazo);
        entity.tFecprorro = this.dateUtils.convertDateTimeFromServer(json.tFecprorro);
        entity.tFecreg = this.dateUtils.convertDateTimeFromServer(json.tFecreg);
        entity.tFecsolic = this.dateUtils.convertDateTimeFromServer(json.tFecsolic);
        entity.tFecsubsan = this.dateUtils.convertDateTimeFromServer(json.tFecsubsan);
        entity.tFecupd = this.dateUtils.convertDateTimeFromServer(json.tFecupd);
        return entity;
    }

    /**
     * Convert a Solicitud to a JSON which can be sent to the server.
     */
    private convert(solicitud: Solicitud): Solicitud {
        const copy: Solicitud = Object.assign({}, solicitud);

        copy.tFecnotif = this.dateUtils.toDate(solicitud.tFecnotif);
        copy.tFecplazo = this.dateUtils.toDate(solicitud.tFecplazo);
        copy.tFecprorro = this.dateUtils.toDate(solicitud.tFecprorro);
        copy.tFecreg = this.dateUtils.toDate(solicitud.tFecreg);
        copy.tFecsolic = this.dateUtils.toDate(solicitud.tFecsolic);
        copy.tFecsubsan = this.dateUtils.toDate(solicitud.tFecsubsan);
        copy.tFecupd = this.dateUtils.toDate(solicitud.tFecupd);

        return copy;
    }

    obtenerlistaSolicitudes(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        const url = SERVER_API_URL + this.resourceDictamenes + '/api/obtenerSolicitud';
        return this.http.get(url + '?', options)
            .map((res: Response) => this.convertResponse(res));
    }

    obtenerlistaSolicitudesPorRuc(ruc: string): Observable<ResponseWrapper> {
        const options = createRequestOption();
        const url = SERVER_API_URL + this.resourceDictamenes + '/api/obtenerSolicitudPorRuc';
        return this.http.get(url + '?ruc=' + ruc, options)
            .map((res: Response) => this.convertResponse(res));
    }

    buscarSolicitudes(ruc: string, razonsocial: string): Observable<ResponseWrapper> {
        const options = createRequestOption();
        const url = SERVER_API_URL + this.resourceDictamenes + '/api/buscarSolicitudes';
        return this.http.get(url + '?&ruc=' + ruc + '&razonSocial=' + razonsocial.toUpperCase() , options)
            .map((res: Response) => this.convertResponse(res));
    }

    eliminarSolicitud(codSolicitud: number): Observable<ResponseWrapper> {
        const options = createRequestOption();
        const url = SERVER_API_URL + this.resourceDictamenes + '/api/eliminarSolicitud';
        return this.http.get(url + '?&codSolicitud=' + codSolicitud, options)
            .map((res: Response) => this.convertResponse(res));
    }
}
