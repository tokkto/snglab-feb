import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../../../shared';
import {
    FormperfilService,
    FormperfilComponent,
    formperfilRoute,
} from './';

const ENTITY_STATES = [
    ...formperfilRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        FormperfilComponent,
    ],
    entryComponents: [
        FormperfilComponent,
    ],
    providers: [
        FormperfilService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayFormperfilModule {}
