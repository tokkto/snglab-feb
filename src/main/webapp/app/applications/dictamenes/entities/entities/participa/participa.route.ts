import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../../../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { ParticipaComponent } from './participa.component';

export const participaRoute: Routes = [
    {
        path: 'participa',
        component: ParticipaComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.participa.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
