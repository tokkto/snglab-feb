import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../../../shared';
import {
    RespinformaService,
    RespinformaComponent,
    respinformaRoute,
} from './';

const ENTITY_STATES = [
    ...respinformaRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        RespinformaComponent,
    ],
    entryComponents: [
        RespinformaComponent,
    ],
    providers: [
        RespinformaService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayRespinformaModule {}
