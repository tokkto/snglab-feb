import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../../../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { FormperfilComponent } from './formperfil.component';

export const formperfilRoute: Routes = [
    {
        path: 'formperfil',
        component: FormperfilComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.formperfil.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
