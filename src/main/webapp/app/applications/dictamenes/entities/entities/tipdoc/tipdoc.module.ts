import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../../../shared';
import {
    TipdocService,
    TipdocComponent,
    tipdocRoute,
} from './';

const ENTITY_STATES = [
    ...tipdocRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        TipdocComponent,
    ],
    entryComponents: [
        TipdocComponent,
    ],
    providers: [
        TipdocService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayTipdocModule {}
