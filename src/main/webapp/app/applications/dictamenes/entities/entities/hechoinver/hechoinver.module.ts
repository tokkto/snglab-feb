import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../../../shared';
import {
    HechoinverService,
    HechoinverComponent,
    hechoinverRoute,
} from './';

const ENTITY_STATES = [
    ...hechoinverRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        HechoinverComponent,
    ],
    entryComponents: [
        HechoinverComponent,
    ],
    providers: [
        HechoinverService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewayHechoinverModule {}
