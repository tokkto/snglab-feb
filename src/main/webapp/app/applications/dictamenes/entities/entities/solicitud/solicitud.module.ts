import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { GatewaySharedModule } from '../../../../../shared';
import {ReporteresService} from '../reporteres/';

import {DialogModule} from 'primeng/primeng';

import {
    SolicitudService,
    SolicitudComponent,
    solicitudRoute,
} from './';

const ENTITY_STATES = [
    ...solicitudRoute,
];

@NgModule({
    imports: [
        GatewaySharedModule,
        RouterModule.forChild(ENTITY_STATES),
        DialogModule,
    ],
    declarations: [
        SolicitudComponent,
    ],
    entryComponents: [
        SolicitudComponent,
    ],
    providers: [
        SolicitudService,
        ReporteresService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GatewaySolicitudModule {}
