import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../../../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { RespinformaComponent } from './respinforma.component';

export const respinformaRoute: Routes = [
    {
        path: 'respinforma',
        component: RespinformaComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'gatewayApp.respinforma.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];
