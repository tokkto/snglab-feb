import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService, JhiLanguageService } from 'ng-jhipster';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../../../shared';
import { Message, MenuItem } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { CalculoliquidacionService } from '../calculo-liquidacion.service';
import { Periodocal } from '../periodocal.model';
import { Datosliquidacion } from '../datosliquidacion.model';
import { Calmes } from '../calmes.model';

@Component({
    selector: 'jhi-cts',
    templateUrl: './cts.component.html',
    styles: ['', '']
})

export class CtsComponent implements OnInit {
    messages: Message[] = [];
    itemsBasico: MenuItem[];
    itemsComision: MenuItem[];
    itemsAlimentacion: MenuItem[];
    itemsHorasExtras: MenuItem[];
    itemsAsignacion: MenuItem[];
    itemsOtrosMontosVariables: MenuItem[];
    itemsOtrosMontosFijos: MenuItem[];
    itemsGratificacion: MenuItem[];
    itemsSel: String;
    messageList: Message[] = [];
    messageListMensual: Message[] = [];
    messageListSemanal: Message[] = [];
    messageListMontosFijos: Message[] = [];
    messageListMontosVariables: Message[] = [];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    block: boolean;
    listaPeriodos: Periodocal[];
    selectedListaPeriodos: Periodocal[];
    periodoCalSel: Periodocal;
    flagCalculo: boolean;
    numPeriodo: Number;
    intervaloFecha: string;
    datosLiquidacion: Datosliquidacion;
    displayCalculoMensual: boolean;
    displayCalculoSemanal: boolean;
    displayMontosFijos: boolean;
    displayMontosVariables: boolean;
    listaCalmes: Calmes[];
    listaCalmesMF: Calmes[];
    listaCalmesMV: Calmes[];

    constructor(
        private eventManager: JhiEventManager,
        private messageService: MessageService,
        private calculoliquidacionService: CalculoliquidacionService
    ) {
    }
    ngOnInit() {
        this.displayCalculoMensual = false;
        this.displayCalculoSemanal = false;
        this.displayMontosFijos = false;
        this.displayMontosVariables = false;
        this.listaPeriodos = [];
        this.itemsSel = '';
        this.listaCalmes = [];
        this.listaCalmesMF = [];
        this.listaCalmesMV = [];
        this.flagCalculo = false;
        this.numPeriodo = 0;
        this.intervaloFecha = '';
        this.block = true;
        this.datosLiquidacion = new Datosliquidacion();
        this.calculoliquidacionService.consultaDatosLiquidacion(3)
            .subscribe(
            (res: Datosliquidacion) => {
                this.datosLiquidacion = res;
                console.log(this.datosLiquidacion);
                this.cargarTramos();
            },
            (res: any) => {
                this.onError(res);
                this.block = false;
            });
        this.itemsBasico = [
            {
                label: 'Semanal', icon: 'fa-clock', command: () => {
                    this.abrirCalculoSemanal();
                    this.itemsSel = 'basico';
                }
            },
            {
                label: 'Mensual', icon: 'fa-clock', command: () => {
                    this.abrirCalculoMensual();
                    this.itemsSel = 'basico';
                }
            }
        ];
        this.itemsAlimentacion = [
            {
                label: 'Semanal', icon: 'fa-clock', command: () => {
                    this.abrirCalculoSemanal();
                    this.itemsSel = 'alimentacion';
                }
            },
            {
                label: 'Mensual', icon: 'fa-clock', command: () => {
                    this.abrirCalculoMensual();
                    this.itemsSel = 'alimentacion';
                }
            }
        ];
        this.itemsAsignacion = [
            {
                label: 'Semanal', icon: 'fa-clock', command: () => {
                    this.abrirCalculoSemanal();
                    this.itemsSel = 'asignacion';
                }
            },
            {
                label: 'Mensual', icon: 'fa-clock', command: () => {
                    this.abrirCalculoMensual();
                    this.itemsSel = 'asignacion';
                }
            }
        ];
        this.itemsComision = [
            {
                label: 'Semanal', icon: 'fa-clock', command: () => {
                    this.abrirCalculoSemanal();
                    this.itemsSel = 'comision';
                }
            },
            {
                label: 'Mensual', icon: 'fa-clock', command: () => {
                    this.abrirCalculoMensual();
                    this.itemsSel = 'comision';
                }
            }
        ];
        this.itemsGratificacion = [
            {
                label: 'Semanal', icon: 'fa-clock', command: () => {
                    this.abrirCalculoSemanal();
                    this.itemsSel = 'gratificacion';
                }
            },
            {
                label: 'Mensual', icon: 'fa-clock', command: () => {
                    this.abrirCalculoMensual();
                    this.itemsSel = 'gratificacion';
                }
            }
        ];
        this.itemsHorasExtras = [
            {
                label: 'Semanal', icon: 'fa-clock', command: () => {
                    this.abrirCalculoSemanal();
                    this.itemsSel = 'horasExtras';
                }
            },
            {
                label: 'Mensual', icon: 'fa-clock', command: () => {
                    this.abrirCalculoMensual();
                    this.itemsSel = 'horasExtras';
                }
            }
        ];
        this.itemsOtrosMontosFijos = [
            {
                label: 'Semanal', icon: 'fa-clock', command: () => {
                    this.abrirCalculoSemanal();
                    this.itemsSel = 'otrosMontosFijos';
                }
            },
            {
                label: 'Mensual', icon: 'fa-clock', command: () => {
                    this.abrirCalculoMensual();
                    this.itemsSel = 'otrosMontosFijos';
                }
            }
        ];
        this.itemsOtrosMontosVariables = [
            {
                label: 'Semanal', icon: 'fa-clock', command: () => {
                    this.abrirCalculoSemanal();
                    this.itemsSel = 'otrosMontosVariables';
                }
            },
            {
                label: 'Mensual', icon: 'fa-clock', command: () => {
                    this.abrirCalculoMensual();
                    this.itemsSel = 'otrosMontosVariables';
                }
            }
        ];
    }

    private cargarTramos() {
        this.block = true;
        this.calculoliquidacionService.consultaTramosDatosLab(
            {
                id: 3
            })
            .subscribe(
            (res: any) => {
                console.log(res);
                this.listaPeriodos = res;
                console.log(this.listaPeriodos);
                this.block = false;
            },
            (res: any) => {
                this.onError(res);
                this.block = false;
            });
    }

    selectPeriodo(periodo: Periodocal) {
        this.numPeriodo = periodo.nNumper;
        this.intervaloFecha = this.formattedDate(periodo.tFecini) + ' - ' + this.formattedDate(periodo.tFecfin);
        this.flagCalculo = true;
        this.periodoCalSel = periodo;
        this.periodoCalSel.nTcomput = (this.periodoCalSel.nTcomput === undefined ? 0 : this.periodoCalSel.nTcomput);
        this.periodoCalSel.nTnocomput = (this.periodoCalSel.nTnocomput === undefined ? 0 : this.periodoCalSel.nTnocomput);
        this.periodoCalSel.nTcomputm = (this.periodoCalSel.nTcomputm === undefined ? 0 : this.periodoCalSel.nTcomputm);
        this.periodoCalSel.nTnocomputm = (this.periodoCalSel.nTnocomputm === undefined ? 0 : this.periodoCalSel.nTnocomputm);
        this.periodoCalSel.nTcomputd = (this.periodoCalSel.nTcomputd === undefined ? 0 : this.periodoCalSel.nTcomputd);
        this.periodoCalSel.nTnocomputd = (this.periodoCalSel.nTnocomputd === undefined ? 0 : this.periodoCalSel.nTnocomputd);
    }

    formattedDate(date: any): string {
        const d = new Date(date);
        let month = String(d.getMonth() + 1);
        let day = String(d.getDate());
        const year = String(d.getFullYear());
        if (month.length < 2) { month = '0' + month; }
        if (day.length < 2) { day = '0' + day; }
        return `${day}/${month}/${year}`;
    }
    abrirCalculoMF() {
        console.log('Entrooo');
        if (this.periodoCalSel === undefined) {
            this.onError({ message: 'Debe seleccionar un periodo' })
        } else {
            this.listaCalmesMF = [];
            this.displayMontosFijos = true;
        }
    }
    agregarItemMF() {
        let flag = true;
        if (this.listaCalmesMF.length > 0) {
            for (const i in this.listaCalmesMF) {
                if (this.listaCalmesMF[i].mes === undefined || this.listaCalmesMF[i].mes.trim() === '') {
                    this.onErrorMontosFijos({ message: 'Debe ingresar la descripcion del concepto remunerativo N° ' + this.listaCalmesMF[i].numero });
                    flag = false;
                    break;
                } else if (this.listaCalmesMF[i].remuneracion === undefined) {
                    this.onErrorMontosFijos({ message: 'Debe ingresar una remuneracion valida para el concepto remunerativo N° ' + this.listaCalmesMF[i].numero });
                    flag = false;
                    break;
                } else {
                    flag = true;
                }
            }
        }
        if (flag) {
            const listaCalmesMF = [...this.listaCalmesMF];
            const obj: Calmes = new Calmes();
            obj.anio = 0;
            obj.mes = '';
            obj.remuneracion = 0;
            obj.numero = 0;
            if (listaCalmesMF.length === 0) {
                obj.numero = 1;
                listaCalmesMF.push(obj);
            } else {
                obj.numero = this.listaCalmesMF.length + 1;
                listaCalmesMF.push(obj);
            }
            this.listaCalmesMF = listaCalmesMF;
        }
    }
    quitarItemMF(item: Calmes) {
        let cont = 0;
        const listaCalmesMF: Calmes[] = [];
        // tslint:disable-next-line:forin
        for (const i in this.listaCalmesMF) {
            if (!(this.listaCalmesMF[i].numero === item.numero)) {
                cont++;
                const itemNuevo: Calmes = new Calmes();
                itemNuevo.anio = 0;
                itemNuevo.mes = this.listaCalmesMF[i].mes;
                itemNuevo.remuneracion = this.listaCalmesMF[i].remuneracion;
                itemNuevo.numero = cont;
                listaCalmesMF.push(itemNuevo);
            }
            console.log(listaCalmesMF);
        }
        this.listaCalmesMF = listaCalmesMF;
    }
    agregarItemMV() {
        let flag = true;
        if (this.listaCalmesMV.length > 0) {
            for (const i in this.listaCalmesMV) {
                if (this.listaCalmesMV[i].mes === undefined || this.listaCalmesMV[i].mes.trim() === '') {
                    this.onErrorMontosFijos({ message: 'Debe ingresar la descripcion del concepto remunerativo N° ' + this.listaCalmesMF[i].numero });
                    flag = false;
                    break;
                } else if (this.listaCalmesMV[i].remuneracion === undefined) {
                    this.onErrorMontosFijos({ message: 'Debe ingresar una remuneracion valida para el concepto remunerativo N° ' + this.listaCalmesMF[i].numero });
                    flag = false;
                    break;
                } else {
                    flag = true;
                }
            }
        }
        if (flag) {
            const listaCalmesMV = [...this.listaCalmesMV];
            const obj: Calmes = new Calmes();
            obj.anio = 0;
            obj.mes = '';
            obj.remuneracion = 0;
            obj.numero = 0;
            if (listaCalmesMV.length === 0) {
                obj.numero = 1;
                listaCalmesMV.push(obj);
            } else {
                obj.numero = this.listaCalmesMV.length + 1;
                listaCalmesMV.push(obj);
            }
            this.listaCalmesMV = listaCalmesMV;
        }
    }
    quitarItemMV(item: Calmes) {
        let cont = 0;
        const listaCalmesMV: Calmes[] = [];
        // tslint:disable-next-line:forin
        for (const i in this.listaCalmesMV) {
            if (!(this.listaCalmesMV[i].numero === item.numero)) {
                cont++;
                const itemNuevo: Calmes = new Calmes();
                itemNuevo.anio = 0;
                itemNuevo.mes = this.listaCalmesMV[i].mes;
                itemNuevo.remuneracion = this.listaCalmesMV[i].remuneracion;
                itemNuevo.numero = cont;
                listaCalmesMV.push(itemNuevo);
            }
            console.log(listaCalmesMV);
        }
        this.listaCalmesMV = listaCalmesMV;
    }
    abrirCalculoMV() {
        console.log('Entrooo');
        if (this.periodoCalSel === undefined) {
            this.onError({ message: 'Debe seleccionar un periodo' })
        } else {
            this.listaCalmes = [];
            this.displayMontosVariables = true;
        }
    }

    abrirCalculoSemanal() {
        if (this.periodoCalSel === undefined) {
            this.onError({ message: 'Debe seleccionar un periodo' })
        } else {
            this.listaCalmes = [];
            this.block = true;
            this.calculoliquidacionService.consultaCalculoSemanal(this.periodoCalSel)
                .subscribe(
                (res: any) => {
                    this.listaCalmes = res;
                    this.block = false;
                    this.displayCalculoSemanal = true;
                },
                (res: any) => {
                    this.onError(res);
                    this.block = false;
                });
        }
    }

    abrirCalculoMensual() {
        if (this.periodoCalSel === undefined) {
            this.onError({ message: 'Debe seleccionar un periodo' })
        } else {
            this.listaCalmes = [];
            this.block = true;
            this.calculoliquidacionService.consultaCalculoMes(this.periodoCalSel)
                .subscribe(
                (res: any) => {
                    this.listaCalmes = res;
                    this.displayCalculoMensual = true;
                    this.block = false;
                },
                (res: any) => {
                    this.onError(res);
                    this.block = false;
                });
        }
    }

    calculoSemanal() {
        if (this.listaCalmes.length > 0) {
            let suma = 0;
            let calculo = 0;
            // tslint:disable-next-line:forin
            for (const i in this.listaCalmes) {
                suma = suma + Number(this.listaCalmes[i].remuneracion);
            }
            calculo = Math.round(Number(suma) / Number(this.listaCalmes.length) * 100) / 100;
            switch (this.itemsSel) {
                case 'basico':
                    this.periodoCalSel.basico = calculo;
                    break;
                case 'comision':
                    this.periodoCalSel.comision = calculo;
                    break;
                case 'alimentacion':
                    this.periodoCalSel.alimentacion = calculo;
                    break;
                case 'horasExtras':
                    this.periodoCalSel.horasExtras = calculo;
                    break;
                case 'asignacion':
                    this.periodoCalSel.asignacion = calculo;
                    break;
                case 'otrosMontosVariables':
                    this.periodoCalSel.otrosMontosVariables = calculo;
                    break;
                case 'otrosMontosFijos':
                    this.periodoCalSel.otrosMontosFijos = calculo;
                    break;
                case 'gratificacion':
                    this.periodoCalSel.gratificacion = calculo;
                    break;
            }
            this.displayCalculoSemanal = false;
        }
    }
    calculoMensual() {
        if (this.listaCalmes.length > 0) {
            let suma = 0;
            let calculo = 0;
            // tslint:disable-next-line:forin
            for (const i in this.listaCalmes) {
                suma = suma + Number(this.listaCalmes[i].remuneracion);
            }
            calculo = Math.round(Number(suma) / Number(this.listaCalmes.length) * 100) / 100;
            switch (this.itemsSel) {
                case 'basico':
                    this.periodoCalSel.basico = calculo;
                    break;
                case 'comision':
                    this.periodoCalSel.comision = calculo;
                    break;
                case 'alimentacion':
                    this.periodoCalSel.alimentacion = calculo;
                    break;
                case 'horasExtras':
                    this.periodoCalSel.horasExtras = calculo;
                    break;
                case 'asignacion':
                    this.periodoCalSel.asignacion = calculo;
                    break;
                case 'otrosMontosVariables':
                    this.periodoCalSel.otrosMontosVariables = calculo;
                    break;
                case 'otrosMontosFijos':
                    this.periodoCalSel.otrosMontosFijos = calculo;
                    break;
                case 'gratificacion':
                    this.periodoCalSel.gratificacion = calculo;
                    break;
            }
            this.displayCalculoMensual = false;
        }
    }
    calculoMontosFijos() {
        let suma = 0;
        if (this.listaCalmesMF.length === 0) {
            this.onErrorMontosFijos({ message: 'Debe ingresar minimo una monto fijo' });
        } else {
            // tslint:disable-next-line:forin
            for (const i in this.listaCalmesMF) {
                if (this.listaCalmesMF[i].mes === undefined || this.listaCalmesMF[i].mes.trim() === '') {
                    this.onErrorMontosFijos({ message: 'Debe ingresar la descripcion del concepto remunerativo N° ' + this.listaCalmesMF[i].numero });
                    break;
                } else if (this.listaCalmesMF[i].remuneracion === undefined) {
                    this.onErrorMontosFijos({ message: 'Debe ingresar una remuneracion valida para el concepto remunerativo N° ' + this.listaCalmesMF[i].numero });
                    break;
                } else {
                    suma = Number(suma) + Number(this.listaCalmesMF[i].remuneracion);
                }
            }
            this.periodoCalSel.otrosMontosFijos = Math.round(Number(suma) * 100) / 100;
            this.displayMontosFijos = false;
        }
    }
    calculoMontosVariables() {
        let suma = 0;
        if (this.listaCalmesMF.length === 0) {
            this.onErrorMontosFijos({ message: 'Debe ingresar minimo una monto variable' });
        } else {
            // tslint:disable-next-line:forin
            for (const i in this.listaCalmesMV) {
                if (this.listaCalmesMV[i].mes === undefined || this.listaCalmesMV[i].mes.trim() === '') {
                    this.onErrorMontosFijos({ message: 'Debe ingresar la descripcion del concepto remunerativo N° ' + this.listaCalmesMF[i].numero });
                    break;
                } else if (this.listaCalmesMV[i].remuneracion === undefined) {
                    this.onErrorMontosFijos({ message: 'Debe ingresar una remuneracion valida para el concepto remunerativo N° ' + this.listaCalmesMF[i].numero });
                    break;
                } else {
                    suma = Number(suma) + Number(this.listaCalmesMV[i].remuneracion);
                }
            }
            this.periodoCalSel.otrosMontosVariables = Math.round(Number(suma) * 100) / 100;
            this.displayMontosVariables = false;
        }
    }
    validarOtrosMontosFijos(event: Event, data: any, entidad: Calmes) {
        if (this.validarIsNumber(data) === false) {
            event.srcElement.nodeValue = '';
            this.block = true;
            // tslint:disable-next-line:forin
            for (const i in this.listaCalmesMF) {
                if (this.listaCalmesMF[i].numero === entidad.numero) {
                    this.listaCalmesMF[i].remuneracion = 0;
                    this.onErrorMontosFijos({ message: 'Debe ingresar valores numericos' });
                    break;
                }
            }
            this.block = false;
        }
    }
    validarOtrosMontosVariables(event: Event, data: any, entidad: Calmes) {
        if (this.validarIsNumber(data) === false) {
            event.srcElement.nodeValue = '';
            this.block = true;
            // tslint:disable-next-line:forin
            for (const i in this.listaCalmes) {
                if (this.listaCalmes[i].numero === entidad.numero) {
                    this.listaCalmes[i].remuneracion = 0;
                    this.onErrorMontosFijos({ message: 'Debe ingresar valores numericos' });
                    break;
                }
            }
            this.block = false;
        }
    }
    validarNumeroMensual(event: Event, data: any, entidad: Calmes) {
        if (this.validarIsNumber(data) === false) {
            event.srcElement.nodeValue = '';
            this.block = true;
            // tslint:disable-next-line:forin
            for (const i in this.listaCalmes) {
                if (this.listaCalmes[i].numero === entidad.numero) {
                    this.listaCalmes[i].remuneracion = 0;
                    this.onErrorMensual({ message: 'Debe ingresar valores numericos' });
                    break;
                }
            }
            this.block = false;
        }
    }
    validarNumeroSemanal(event: Event, data: any, entidad: Calmes) {
        if (this.validarIsNumber(data) === false) {
            event.srcElement.nodeValue = '';
            // tslint:disable-next-line:forin
            this.block = true;
            for (const i in this.listaCalmes) {
                if (this.listaCalmes[i].numero === entidad.numero) {
                    this.listaCalmes[i].remuneracion = 0;
                    this.onErrorSemanal({ message: 'Debe ingresar valores numericos' });
                    break;
                }
            }
            this.block = false;
        }
    }
    validarConceptoRemunerativo(concepto: String, data: any) {
        if (data === undefined) {
        } else if (this.validarIsNumber(data) === false) {
            switch (concepto) {
                case 'basico': this.onError({ message: 'El valor ingresado para el basico no es un dato valido' });
                    this.periodoCalSel.basico = 0.00;
                    break;
                case 'comision': this.onError({ message: 'El valor ingresado para la comision no es un dato valido' });
                    this.periodoCalSel.comision = 0.00;
                    break;
                case 'alimentacion': this.onError({ message: 'El valor ingresado para la alimentacion no es un dato valido' });
                    this.periodoCalSel.alimentacion = 0.00;
                    break;
                case 'horasExtras': this.onError({ message: 'El valor ingresado para las horas extras no es un dato valido' });
                    this.periodoCalSel.horasExtras = 0.00;
                    break;
                case 'asignacion': this.onError({ message: 'El valor ingresado para la asignacion no es un dato valido' });
                    this.periodoCalSel.asignacion = 0.00;
                    break;
                case 'otrosMontosVariables': this.onError({ message: 'El valor ingresado para los otros montos variables no es un dato valido' });
                    this.periodoCalSel.otrosMontosVariables = 0.00;
                    break;
                case 'otrosMontosFijos': this.onError({ message: 'El valor ingresado para los otros montos fijos no es un dato valido' });
                    this.periodoCalSel.otrosMontosFijos = 0.00;
                    break;
                case 'gratificacion': this.onError({ message: 'El valor ingresado para la gratificacion no es un dato valido' });
                    this.periodoCalSel.gratificacion = 0.00;
                    break;
            }
        }
    }

    validarIsNumber(n: any): boolean {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }

    calcularCTS() {
        console.log('Calular CTS');
        console.log(this.periodoCalSel);
        const tcomp = (Number(this.periodoCalSel.nTcomput) * 360) + (Number(this.periodoCalSel.nTcomputm) * 30) + Number(this.periodoCalSel.nTcomputd)
        const tnocomp = (Number(this.periodoCalSel.nTnocomput) * 360) + (Number(this.periodoCalSel.nTnocomputm) * 30) + Number(this.periodoCalSel.nTnocomputd)
        if ((tcomp - tnocomp) < 0) {
            this.onError({ message: 'El tiempo no computable no debe ser mayor al tiempo de servicio' });
        } else {
            let rcmSuma: Number = 0;
            if (this.periodoCalSel.basico === undefined) {
                this.periodoCalSel.basico = 0;
            }
            if (this.periodoCalSel.alimentacion === undefined) {
                this.periodoCalSel.alimentacion = 0;
            }
            if (this.periodoCalSel.asignacion === undefined) {
                this.periodoCalSel.asignacion = 0;
            }
            if (this.periodoCalSel.otrosMontosFijos === undefined) {
                this.periodoCalSel.otrosMontosFijos = 0;
            }
            if (this.periodoCalSel.comision === undefined) {
                this.periodoCalSel.comision = 0;
            }
            if (this.periodoCalSel.horasExtras === undefined) {
                this.periodoCalSel.horasExtras = 0;
            }
            if (this.periodoCalSel.otrosMontosVariables === undefined) {
                this.periodoCalSel.otrosMontosVariables = 0;
            }
            if (this.periodoCalSel.gratificacion === undefined) {
                this.periodoCalSel.gratificacion = 0;
            }
            if (this.periodoCalSel.rcm === undefined) {
                this.periodoCalSel.rcm = 0;
            }
            if (this.periodoCalSel.deposito === undefined) {
                this.periodoCalSel.deposito = 0;
            }
            if (this.periodoCalSel.interes === undefined) {
                this.periodoCalSel.interes = 0;
            }
            if (this.periodoCalSel.cts === undefined) {
                this.periodoCalSel.cts = 0;
            }
            rcmSuma = Number(this.periodoCalSel.basico) +
                Number(this.periodoCalSel.asignacion) +
                Number(this.periodoCalSel.otrosMontosFijos) +
                Number(this.periodoCalSel.comision) +
                Number(this.periodoCalSel.horasExtras) +
                Number(this.periodoCalSel.otrosMontosVariables) +
                Number(this.periodoCalSel.gratificacion) +
                Number(this.periodoCalSel.basico) +
                Number(this.periodoCalSel.basico) +
                Number(this.periodoCalSel.basico) +
                Number(this.periodoCalSel.basico) +
                Number(this.periodoCalSel.basico) +
                Number(this.periodoCalSel.basico) +
                Number(this.periodoCalSel.basico);

            this.periodoCalSel.rcm = Math.round(Number(rcmSuma) * 100) / 100;
            this.periodoCalSel.deposito = rcmSuma;
            this.periodoCalSel.interes = 0;
            this.periodoCalSel.cts = 0;
        }
    }

    calculoTiempEfectivo(event, int: Number) {
        console.log(event);
        // tslint:disable-next-line:no-debugger
        debugger;
        const resultado = this.calculoliquidacionService.calculoTiempoEfectivo(
            (this.periodoCalSel.nTcomput === undefined ? 0 : this.periodoCalSel.nTcomput),
            (this.periodoCalSel.nTnocomput === undefined ? 0 : this.periodoCalSel.nTnocomput),
            (this.periodoCalSel.nTcomputm === undefined ? 0 : this.periodoCalSel.nTcomputm),
            (this.periodoCalSel.nTnocomputm === undefined ? 0 : this.periodoCalSel.nTnocomputm),
            (this.periodoCalSel.nTcomputd === undefined ? 0 : this.periodoCalSel.nTcomputd),
            (this.periodoCalSel.nTnocomputd === undefined ? 0 : this.periodoCalSel.nTnocomputd)
        );
        this.periodoCalSel.nTefectivoDia = Number(resultado[0]);
        this.periodoCalSel.nTefectivoMes = Number(resultado[1]);
        this.periodoCalSel.nTefectivoAnio = Number(resultado[2]);
    }

    private onErrorSemanal(error: any) {
        this.messageListSemanal = [];
        this.messageListSemanal.push({ severity: 'error', summary: 'Mensaje de Error', detail: error.message });
    }
    private onErrorMensual(error: any) {
        this.messageListMensual = [];
        this.messageListMensual.push({ severity: 'error', summary: 'Mensaje de Error', detail: error.message });
    }
    private onErrorMontosFijos(error: any) {
        this.messageListMontosFijos = [];
        this.messageListMontosFijos.push({ severity: 'error', summary: 'Mensaje de Error', detail: error.message });
    }
    private onErrorMontosVariables(error: any) {
        this.messageListMontosVariables = [];
        this.messageListMontosVariables.push({ severity: 'error', summary: 'Mensaje de Error', detail: error.message });
    }
    private onError(error: any) {
        console.log(error);
        this.messageList = [];
        this.messageList.push({ severity: 'error', summary: 'Mensaje de Error', detail: error.message });
    }
}
