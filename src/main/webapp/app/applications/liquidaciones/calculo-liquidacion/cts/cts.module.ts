import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GatewaySharedModule } from '../../../../shared';
import { MessagesModule, MessageModule, DialogModule } from 'primeng/primeng';
import { MessageService } from 'primeng/components/common/messageservice';
import { BlockUIModule } from 'primeng/primeng';
import { CtsComponent } from './cts.component';
import { CalculoliquidacionService } from '../calculo-liquidacion.service';
import { GrowlModule, DataTableModule, SharedModule, ButtonModule, SpinnerModule, InputMaskModule, SplitButtonModule } from 'primeng/primeng';
/*
const ENTITY_STATES = [
    ...RegistroAtencionRoute
];
*/

@NgModule({
    imports: [
        MessagesModule,
        MessageModule,
        DialogModule,
        GatewaySharedModule,
        BlockUIModule,
        DataTableModule,
        SharedModule,
        ButtonModule,
        SpinnerModule,
        InputMaskModule,
        SplitButtonModule,
        GrowlModule
        /*RouterModule.forChild(ENTITY_STATES)*/
    ],
    declarations: [
        CtsComponent
    ],
    /*entryComponents: [
        RegistroAtencionComponent
    ],*/
    providers: [
        MessageService,
        CalculoliquidacionService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TrabajadorModule { }
