import { BaseEntity } from '../../../shared';

export class Periodocal {
    constructor(
        public id?: Number,
        public nCalper?: Number,
        public nCalper2?: Number,
        public nNumper?: Number,
        public tFecini?: Date,
        public tFecfin?: Date,
        public nTnocomput?: Number,
        public nTcomput?: Number,
        public nCodhijo1?: Number,
        public nCodhijo2?: Number,
        public nDgozados?: Number,
        public nDadeudos?: Number,
        public nAnobase?: Number,
        public nCodtipcal?: Number,
        public nTnocomputd?: Number,
        public nTnocomputm?: Number,
        public nTcomputd?: Number,
        public nTcomputm?: Number,
        // Por confirmar el calculo
        public basico?: Number,
        public alimentacion?: Number,
        public asignacion?: Number,
        public otrosMontosFijos?: Number,
        public comision?: Number,
        public horasExtras?: Number,
        public otrosMontosVariables?: Number,
        public gratificacion?: Number,
        public rcm?: Number,
        public deposito?: Number,
        public interes?: Number,
        public cts?: Number,
        public nTefectivoAnio?: Number,
        public nTefectivoMes?: Number,
        public nTefectivoDia?: Number
    ) {
        this.basico = 0;
        this.alimentacion = 0;
        this.asignacion = 0;
        this.otrosMontosFijos = 0;
        this.comision = 0;
        this.horasExtras = 0;
        this.otrosMontosVariables = 0;
        this.gratificacion = 0;
        this.rcm = 0;
        this.deposito = 0;
        this.interes = 0;
        this.cts = 0;
        this.nTefectivoAnio = 0;
        this.nTefectivoMes = 0;
        this.nTefectivoDia = 0;
    }
}
