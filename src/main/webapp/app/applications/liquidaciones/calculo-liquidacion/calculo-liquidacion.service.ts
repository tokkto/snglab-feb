import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { JhiDateUtils } from 'ng-jhipster';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Periodocal } from './periodocal.model';
import { Datosliquidacion } from './datosliquidacion.model';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class CalculoliquidacionService {
    private resourceDatosLiquidacion = '/liquidaciones/api/datosliquidacion/';
    private resourceTramosCTS = '/liquidaciones/api/tramoscts';
    private resourceCalculoMes = '/liquidaciones/api/calmesremuneracion';
    private resourceCalculoSemanal = '/liquidaciones/api/calsemanaremuneracion';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    consultaTramosDatosLab(serialize: any): Observable<Periodocal[]> {
        return this.http.post(`${this.resourceTramosCTS}`, serialize).map((res: Response) => {
            return res.json();
        });
    }

    consultaDatosLiquidacion(serialize: any): Observable<Datosliquidacion> {
        return this.http.get(`${this.resourceDatosLiquidacion}/${serialize}`).map((res: Response) => {
            return res.json();
        });
    }

    consultaCalculoMes(serialize: any): Observable<Datosliquidacion> {
        return this.http.post(`${this.resourceCalculoMes}`, serialize).map((res: Response) => {
            return res.json();
        });
    }

    consultaCalculoSemanal(serialize: any): Observable<Datosliquidacion> {
        return this.http.post(`${this.resourceCalculoMes}`, serialize).map((res: Response) => {
            return res.json();
        });
    }

    calculoTiempoEfectivo(
        anioC: Number,
        anioNC: Number,
        mesC: Number,
        mesNC: Number,
        diaC: Number,
        diaNC: Number,
    ): Number[] {
        const lista: Number[] = [];
        let D: Number = 0;
        let M: Number = 0;
        let A: Number = 0;
        if (diaC < diaNC) {
            diaC = Number(diaC) + Number(30);
            if (mesC > 0) {
                mesC = Number(mesC) - 1;
            } else {
                mesC = 11;
                anioC = Number(anioC) - 1;
            }
        }
        D = Number(diaC) - Number(diaNC);
        if (mesC < mesNC) {
            mesC = Number(mesC) + 12;
            anioC = Number(anioC) - 1;
        }

        M = Number(mesC) - Number(mesNC);
        A = Number(anioC) - Number(anioNC);

        lista.push(D);
        lista.push(M);
        lista.push(A);

        return lista;
    }
}
